//
//  ViewController.swift
//  Venues
//
//  Created by Alex Ignatenko on 11/03/2017.
//  Copyright © 2017 Alex Ignatenko. All rights reserved.
//

import UIKit
import SDWebImage

class VenuesViewController: UICollectionViewController, DataFetcherDelegate {

    private static let PhotoCellIdentifier = "PhotoCellIdentifier"

    private lazy var dataFetcher: DataFetcher = {
        let fetcher = DataFetcher()
        fetcher.delegate = self
        return fetcher
    }()

    private var photos: [Photo] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        dataFetcher.startFetchingData()
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VenuesViewController.PhotoCellIdentifier, for: indexPath) as! PhotoCell

        let photo = photos[indexPath.item]

        let dimention = Int(cell.imageView.bounds.size.width)

        cell.imageView.sd_setShowActivityIndicatorView(true)
        cell.imageView.sd_setImage(with: photo.photoUrl(with: dimention))

        return cell
    }

    // MARK: DataFetcherDelegate

    func didUpdatePhotos(fetcher: DataFetcher, photos: [Photo]) {
        self.photos = photos
        self.collectionView?.reloadData()
    }

    func didFailUpdatePhotos(fetcher: DataFetcher, error: Error) {
        let alert = UIAlertController(
            title: "Oops",
            message: "Unfortunately at least part of the photo content cannot be seen at the moment",
            preferredStyle: .alert
        )
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)

        self.present(alert, animated: true, completion: nil)
    }
}

