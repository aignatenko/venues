//
//  AppDelegate.swift
//  Venues
//
//  Created by Alex Ignatenko on 11/03/2017.
//  Copyright © 2017 Alex Ignatenko. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        return true
    }
}

