//
//  Photo.swift
//  Venues
//
//  Created by Alex Ignatenko on 11/03/2017.
//  Copyright © 2017 Alex Ignatenko. All rights reserved.
//

import Foundation

struct Photo {
    let id: String
    let prefix: String
    let suffix: String

    func photoUrl(with dimension: Int) -> URL? {
        let stringUrl = prefix + "\(dimension)x\(dimension)" + suffix
        return URL(string: stringUrl)
    }
}

extension Photo {
    init?(json: JSON) {
        guard
            let id = json["id"] as? String,
            let prefix = json["prefix"] as? String,
            let suffix = json["suffix"] as? String
            else { return nil }

        self.id = id
        self.prefix = prefix
        self.suffix = suffix
    }

    static func all(forVenue id: String) -> Resource<[Photo]> {
        let preprocess = { (json: JSON?) -> Any? in
            guard let response = json?["response"] as? JSON,
                let photos = response["photos"] as? JSON,
                let items = photos["items"] as? [JSON]
                else { return nil }

            return items
        }

        return Resource(
            path: "venues/\(id)/photos",
            method: .get,
            preprocessJson: preprocess,
            transform: Photo.init
        )
    }
}
