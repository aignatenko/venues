//
//  DataSource.swift
//  Venues
//
//  Created by Alex Ignatenko on 12/03/2017.
//  Copyright © 2017 Alex Ignatenko. All rights reserved.
//

import Foundation
import CoreLocation

protocol DataFetcherDelegate: class {
    func didUpdatePhotos(fetcher: DataFetcher, photos: [Photo])
    func didFailUpdatePhotos(fetcher: DataFetcher, error: Error)
}

final class DataFetcher: NSObject, CLLocationManagerDelegate {

    enum FetchingError: Error {
        case emptyContent
    }

    weak var delegate: DataFetcherDelegate? = nil

    private var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    private var location: CLLocation? = nil

    private var photos: [Photo] = []
    private var photosFethingError: Error? = nil
    private var currentPhotoTasks: [URLSessionTask] = []

    private lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        manager.distanceFilter = 20
        manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        manager.requestWhenInUseAuthorization()
        return manager
    }()

    // MARK: CLLocationManagerDelegate

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            if self.location?.distance(from: location) ?? 0.1 > 0 {
                self.location = location

                self.currentPhotoTasks.forEach{ $0.cancel() }
                self.currentPhotoTasks = []

                mutex.wait()
                self.photos = []
                mutex.signal()

                fetchPhotos(for: location)
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }

    // MARK: Photos Fetching

    func startFetchingData() {
        self.locationManager.startUpdatingLocation()
    }

    let mutex = DispatchSemaphore(value: 1)

    func fetchPhotos(for location: CLLocation) {
        let latitude = location.coordinate.latitude
        let longitude = location.coordinate.longitude

        let limit = 50

        // fetch harcoded 100 photos

        for offset in [0, 50] {
            let parameters = Venue.ExploreParameters(
                latitude: latitude,
                longitude: longitude,
                limit: limit,
                offset: offset
            )

            let task = WebService.load(Venue.explore(with: parameters)) { venues, error in

                let photos = venues?.flatMap { $0.photos } ?? []

                self.mutex.wait()

                if photos.count > 0 {
                    self.photosFethingError = nil
                    print("\(photos.count) photos loaded successfully")
                } else {
                    self.photosFethingError = error ?? FetchingError.emptyContent
                    print("failed to load photos")
                }

                self.photos.append(contentsOf: photos)

                self.mutex.signal()

                DispatchQueue.main.async {
                    if let error = self.photosFethingError {
                        self.delegate?.didFailUpdatePhotos(fetcher: self, error: error)
                    } else {
                        self.delegate?.didUpdatePhotos(fetcher: self, photos: self.photos)
                    }
                }
            }
            if let task = task {
                self.currentPhotoTasks.append(task)
            }
        }
    }
}
