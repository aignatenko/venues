//
//  Types.swift
//  Venues
//
//  Created by Alex Ignatenko on 11/03/2017.
//  Copyright © 2017 Alex Ignatenko. All rights reserved.
//

import Foundation

typealias JSON = Dictionary<String, Any>
