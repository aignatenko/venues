//
//  PhotoCell.swift
//  Venues
//
//  Created by Alex Ignatenko on 13/03/2017.
//  Copyright © 2017 Alex Ignatenko. All rights reserved.
//

import UIKit

final class PhotoCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
}
