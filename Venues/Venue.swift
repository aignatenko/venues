//
//  Venue.swift
//  Venues
//
//  Created by Alex Ignatenko on 11/03/2017.
//  Copyright © 2017 Alex Ignatenko. All rights reserved.
//

import Foundation

struct Venue {
    let id: String
    let name: String
    let photos: [Photo]
}

extension Venue {
    init?(json: JSON) {
        guard
            let id = json["id"] as? String,
            let name = json["name"] as? String
            else { return nil }

        self.id = id
        self.name = name

        let photos = json["photos"] as? JSON ?? [:]
        let groups = photos["groups"] as? [JSON] ?? []
        let photoItems = groups.flatMap { $0["items"] as? [JSON] }.flatMap { $0 }
        self.photos = photoItems.flatMap(Photo.init(json:))
    }
}

extension Venue {

    struct ExploreParameters {
        let latitude: Double
        let longitude: Double
        let limit: Int
        let offset: Int

        var dictionaryValue: [String: Any] {
            return [
                "ll": "\(latitude), \(longitude)",
                "intent" : "checkin",
                "venuePhotos" : "1",
                "limit": limit,
                "offset": offset
            ]
        }
    }

    static func explore(with parameters: ExploreParameters) -> Resource<[Venue]> {
        let preprocess = { (json: JSON?) -> Any? in
            guard let response = json?["response"] as? JSON,
                let groups = response["groups"] as? [JSON]
                else { return nil }

            let items = groups.flatMap { $0["items"] as? [JSON] }.flatMap { $0 }
            return items.flatMap { $0["venue"] }
        }

        return Resource(
            path: "venues/explore",
            method: .get,
            parameters: parameters.dictionaryValue,
            preprocessJson: preprocess,
            transform: Venue.init
        )
    }
}
