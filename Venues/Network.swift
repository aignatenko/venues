//
//  Network.swift
//  Venues
//
//  Created by Alex Ignatenko on 11/03/2017.
//  Copyright © 2017 Alex Ignatenko. All rights reserved.
//

import Foundation

enum HttpMethod<Body> {
    case get
    case post(Body)
}

extension HttpMethod {
    var stringValue: String {
        switch self {
        case .get: return "GET"
        case .post: return "POST"
        }
    }

    func map<B>(transform: (Body) -> B) -> HttpMethod<B> {
        switch self {
        case .get:
            return .get
        case .post(let body):
            return .post(transform(body))
        }
    }
}

struct Resource<A> {
    let path: String
    let method: HttpMethod<Data>
    let parameters: [String: Any]
    let parse: (Data) -> A?
}

extension Resource {

    static private func convert<A, E>(_ any: Any, _ transform: (JSON) -> E?) -> A? {
        if let array = any as? Array<JSON> {
            let mapped = array.flatMap(transform)
            return mapped as? A
        } else if let value = any as? JSON {
            return transform(value) as? A
        } else {
            return nil
        }
    }

    init(path: String,
         method: HttpMethod<Any> = .get,
         parameters: [String: Any] = [:],
         preprocessJson: @escaping (JSON?) -> Any?,
         parseJson: @escaping (Any) -> A?)
    {
        self.path = path
        self.method = method.map { json in
            return try! JSONSerialization.data(withJSONObject: json, options: [])
        }
        self.parameters = parameters
        self.parse = { data in
            let json = try? JSONSerialization.jsonObject(with: data, options: [])
            let preprocessedJson: Any?
            if let json = json as? JSON {
                preprocessedJson = preprocessJson(json)
            } else {
                preprocessedJson = json
            }
            return preprocessedJson.flatMap(parseJson)
        }
    }

    init<E>(path: String,
         method: HttpMethod<Any> = .get,
         parameters: [String: Any] = [:],
         preprocessJson: @escaping (JSON?) -> Any?,
         transform: @escaping (JSON) -> E?)
    {
        self.init(
            path: path,
            method: method,
            parameters: parameters,
            preprocessJson: preprocessJson,
            parseJson: { Resource.convert($0, transform) })
    }
}

struct ParametersEncoder {
    static func encode(parameters: [String: Any]) -> String {
        let parametersList: [String] = parameters.flatMap { (key: String, value: Any) in
            if let value = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                return "\(key)=\(value)"
            } else {
                return nil
            }
        }
        return parametersList.joined(separator: "&")
    }
}

final class WebService {

    enum RequestError: Error {
        case clientError
        case serverError
    }

    private struct Api {
        static let Url = "https://api.foursquare.com/v2/"
        static let ClientID = "Y2QRWGE1C0Z2FBD5FABCZIKR0MDMEYHBIF0IVZRWSNMBAJZK"
        static let ClientSecred = "YCOQPA20R0M4ZD0TIMMTTRR0AAJZABBQXCNZA13SHXAW1UBP"
        static let VerifiedDate = "20170312"
    }

    private static let session: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = ["Content-Type": "application/json"]
        return URLSession(configuration: configuration)
    }()

    @discardableResult
    class func load<A>(_ resource: Resource<A>, completion: @escaping (A?, Error?) -> ()) -> URLSessionTask? {

        let parameters = [
            "v": Api.VerifiedDate,
            "client_id" : Api.ClientID,
            "client_secret" : Api.ClientSecred,
        ].merged(other: resource.parameters)

        let stringUrl = Api.Url + resource.path + "?" +
            ParametersEncoder.encode(parameters: parameters)

        guard let url = URL(string: stringUrl) else { return nil }

        let request = NSMutableURLRequest(url: url)
        request.httpMethod = resource.method.stringValue
        if case let .post(data) = resource.method {
            request.httpBody = data
        }

        print("\(request.httpMethod) \(request.url)")

        let task = session.dataTask(with: request as URLRequest) { data, response, error in
            guard let response = response as? HTTPURLResponse else { return }

            if let url = response.url {
                print("\(response.statusCode) \(url)")
                if let error = error {
                    print("Error: \(error)")
                }
            }

            switch response.statusCode {
            case 200...299:
                completion(data.flatMap(resource.parse), nil)
            case 400...499:
                completion(nil, RequestError.clientError)
            case 500...599:
                completion(nil, RequestError.serverError)
            default: break
            }
        }
        task.resume()

        return task
    }
}
