//
//  Dictionary+Merge.swift
//  Venues
//
//  Created by Alex Ignatenko on 13/03/2017.
//  Copyright © 2017 Alex Ignatenko. All rights reserved.
//

extension Dictionary {
    mutating func merge(other: Dictionary) {
        for (key, value) in other {
            self.updateValue(value, forKey: key)
        }
    }

    func merged(other: Dictionary) -> Dictionary {
        var copy = self
        copy.merge(other: other)
        return copy
    }
}
